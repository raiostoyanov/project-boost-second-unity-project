# Project-Boost-Second-Unity-Project

This is the second time I'm working with the Unity System and I'm trying to make this one a bit more complex - add some levels, some particles, some audio files and some basic lighting.

Again, you are supposed to try and make it to the Finish Tile, but this time you are in space, with a rocket. Also the obstacles you hit that are part of the environment, makes you explode and take you back at the Start Tile of the Level you are currently on.
Once you reach the Finish Tile, the game takes you to the next level.

Thrust - Space Key
Rotate left and right - A and D Keys

Once you finish the levels it will take you back to the first one. From then on you can hit Escape and quit the game.
